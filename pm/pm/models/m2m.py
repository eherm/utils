"""This has the models for the many to many tables"""

from __future__ import annotations

from pydantic import BaseModel


class StartIssue(BaseModel):
    """StartIssue base model"""

    issue_id: int
    sprint_id: int

    @classmethod
    def start_issue(cls, data: dict) -> StartIssue:
        return cls.parse_obj(data)
