"""Base models for spork rest api resources"""

from __future__ import annotations

from typing import Optional, List, Dict, Union

from pydantic import BaseModel


class SprintRelease(BaseModel):
    estimatedPoints: int
    sprints: List[str]

class SprintQuarter(BaseModel):
    estimatedPoints: int
    sprints: List[str]

class ECRAssignment(BaseModel):
    name: str
    estimates: Optional[Dict[str, int]]

class SprintDetails(BaseModel):
    completedVelocity: int
    plannedVelocity: int
    blockers: int
    releases: Optional[Dict[str, Dict[str, int]]]

class SprintData(BaseModel):
    sprints: Dict[str, SprintDetails]
    releases: Dict[str, SprintRelease]
    quarters: Dict[str, SprintQuarter]
    ecrs: Dict[str, ECRAssignment]

class SWData(BaseModel):
    sw: SprintData

    @classmethod
    def load_from_dict(cls, data_dict: dict):
        return cls(**data_dict)

    @classmethod
    def load(cls, json_data: str):
        data_dict = json.loads(json_data)
        return cls(**data_dict)

    def dump(self, indent: int = 2):
        return json.dumps(self.dict(), indent=indent)

