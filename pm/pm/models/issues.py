"""Base models for spork rest api resources"""

from __future__ import annotations

from datetime import date, datetime
from typing import Optional

from pydantic import BaseModel


class Issue(BaseModel):
    """Spork issue base model

    NOTE: The order of the fields matter and should
    match the order of the sql schema.
    """

    id: int
    title: str
    link: str
    ecr: Optional[str] = None
    closed: bool = False
    updated_at: datetime
    estimated_points: Optional[int] = None

    @classmethod
    def issue(cls, data: dict) -> Issue:
        return cls.parse_obj(data)

    def to_md(self) -> str:
        """Returns the title and link in a markdown format

        Returns: a str in format [title](link)
        """
        return "[{}]({})".format(self.title, self.link)




class Sprint(BaseModel):
    """Sprint base model, based on spork group milestones"""

    sprint_id: int
    sprint_name: str
    points_attempted: Optional[int] = 0
    points_completed: Optional[int] = 0
    end_date: Optional[date] = None
    link: str
    release: Optional[str] = None

    @classmethod
    def sprint(cls, data: dict) -> Sprint:
        return cls.parse_obj(data)
