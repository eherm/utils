from __future__ import annotations

from datetime import date

from pydantic import BaseModel

from typing import Optional


class Release(BaseModel):
    """Release base model"""

    id: str
    code_freeze: date
    start_date: date
    end_date: date

    @classmethod
    def release(cls, data: dict) -> Release:
        return cls.parse_obj(data)

    @classmethod
    def release_from_db(cls, data: dict) -> Release:
        release_dict = {
            "id": data.get("id"),
            "code_freeze": data.get("codeFreeze")
        }


class Ecr(BaseModel):
    """ECR base model"""
    ecr_id: str
    title: str
    approved: bool
    release: Optional[str] = None
    closed: bool
    estimated_points: Optional[int] = None

    @classmethod
    def ecr(cls, data: dict) -> Ecr:
        return cls.parse_obj(data)




