import typer
from ..spork.api import Spork
from typing_extensions import Annotated
from rich import print, print_json
from typing import Optional
from ..graph.graph import run as run_graph
from ..data.data import Data

app = typer.Typer()

@app.callback()
def callback():
    """
    Project management app for spork
    """

@app.command()
def issues(sprint: Annotated[str, typer.Option(help="The name of the Spork milestone")]):
    """
    Get the issues
    """
    typer.echo("Getting issues ...")
    spork = Spork()
    issues = spork.issues_from_milestone(sprint)


@app.command()
def points(sprint: Annotated[str, typer.Option(help="The name of the Spork milestone")]):
    """
    Get the points in the sprint
    """
    typer.echo("Getting points ...")
    spork = Spork()
    points = spork.points_in_milestone_by_state(sprint)
    print(points)

@app.command()
def ecr(sprint: Annotated[str, typer.Option(help="The name of the Spork milestone")]):
    """
    Get the points in the sprint by ecr
    """
    typer.echo("Getting points ...")
    spork = Spork()
    #points = spork.points_in_milestone_by_ecr(sprint)
    points = spork.points_with_ecr_in_milestone_by_release(sprint)
    print(points)

@app.command()
def sprints():
    """
    Get the milestones
    """
    typer.echo("Getting sprint data ...")


@app.command()
def graphs():
    """
    Generate the graphs
    """
    run_graph()

@app.command()
def data(update: Annotated[bool, typer.Option("-u", help="Update the data file")] = False,
         sprint: Annotated[str, typer.Option("-s", help="Update only the sprint specified")] = None,
         completed: Annotated[bool, typer.Option("-c", help="Update the completed and ecr points with the data from spork")] = False,
         blockers: Annotated[int, typer.Option("-b", help="Update the blockers")] = -1,
         planned: Annotated[int, typer.Option("-p", help="Update the planned points")] = -1,
         closed: Annotated[bool, typer.Option("-d", help="True -> Only report on issues that are closed")] = False
         ):

    d = Data()
    if not update:
        data = d.get_file()
        print_json(data.json())
        exit(0)
    if not sprint:
        print("You must define a sprint when using -u")
        exit(0)
    kwargs = {}
    sprint_number = sprint.split(" ")[1]
    if completed:
        spork = Spork()
        points = spork.points_in_milestone_by_state(sprint)
        if closed:
            kwargs.update({'completed_velocity': points.get('closed')})
        else:
            kwargs.update({'completed_velocity': points.get('total')})
        release = spork.points_with_ecr_in_milestone_by_release(sprint, closed=closed)
        kwargs.update({'releases': release})

    if blockers > -1:
        kwargs.update({'blockers': blockers})

    if planned > -1:
        kwargs.update({'planned_velocity': planned})

    updated = d.update_sprint(sprint_number, **kwargs)
    print_json(updated.json())
    exit(0)


