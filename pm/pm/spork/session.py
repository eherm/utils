"""Requests session objects for spork api
"""

import os

import requests
from urllib3.exceptions import InsecureRequestWarning

from pm import config, token


class SporkSession:
    """Class that holds session for spork api"""

    def __init__(self):
        """Sets up the requests session"""

        spork_token = (
            os.getenv("SPORK_TOKEN")
            if os.getenv("SPORK_TOKEN")
            else token.get("spork_token")
        )
        if not spork_token:
            raise EnvironmentError

        self.session = requests.Session()
        headers = {"PRIVATE-TOKEN": spork_token}
        self.session.verify = False
        requests.packages.urllib3.disable_warnings(category=InsecureRequestWarning)
        self.session.headers.update(headers)

    @property
    def url(self) -> str:
        return config.get("base_url")
