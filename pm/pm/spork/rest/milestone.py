"""Wrapper around the spork rest api milestones resource"""

from pm import config

from . import logger

from typing import Optional
from ...models.issues import Sprint
from ..session import SporkSession


class SporkMilestones:
    """Wrapper around the spork milestones"""

    def __init__(self):
        ss = SporkSession()
        self.session = ss.session
        self.horizon_gid = config.get("horizon_gid")
        self.url = "{}/groups/{}/milestones".format(ss.url, self.horizon_gid)

    def get_milestones(self) -> list[dict]:
        """Using the spork api, get all the group milestones

        Returns: a json load of the json data from the spork api
        """
        url = self.url
        resp = self.session.get(url)
        if resp.status_code != 200:
            logger.error(
                "get milestones status code is {}, expected 200".format(
                    resp.status_code
                )
            )
            exit(1)

        milestones = resp.json()
        more = False if resp.links.get("next") == None else True
        prev_resp = resp

        while more:
            curr_resp = self.session.get(prev_resp.links.get("next").get("url"))
            if resp.status_code != 200:
                logger.error(
                    "in while; get milestones status code is {}, expecting 200".format(
                        resp.status_code
                    )
                )
                exit(1)

            milestones = milestones + curr_resp.json()
            prev_resp = curr_resp
            more = False if curr_resp.links.get("next") == None else True

        return milestones

    def filter_milestones(self, milestones: list[dict]) -> list[Sprint]:
        """Using the milestones data from the sprok api, Create and return a list of Sprint objects

        Args:
            milestones: the milestone data downloaded from the spork api

        Returns: a list of Sprint objects
        """
        filtered_milestones: list[Sprint] = []
        for milestone in milestones:
            milestone_dict: dict = {}
            milestone_dict["sprint_id"] = milestone.get("id")
            milestone_dict["sprint_name"] = milestone.get("title")
            milestone_dict["end_date"] = milestone.get("due_date")
            milestone_dict["link"] = milestone.get("web_url")

            filtered_milestones.append(Sprint.sprint(milestone_dict))

        return filtered_milestones

    def get_sprint(self, milestone_title: str) -> Optional[Sprint]:
        """Given the milestone name, return the Sprint object
        """
        for milestone in self.get_milestones():
            if milestone.get("title") and milestone.get("title") == milestone_title:
                logger.debug(milestone)
                sprints = self.filter_milestones([milestone])
                if len(sprints) == 1:
                    return sprints[0]
                return None
        return None

    def get_milestone_issues(self, milestone: str):
        sprint = self.get_sprint(milestone)

        url = "{}/{}/issues".format(self.url, sprint.sprint_id)

        resp = self.session.get(url)
        if resp.status_code != 200:
            logger.error(
                "get milestone issues status code is {}, expected 200".format(
                    resp.status_code
                )
            )
            exit(1)

        issues = resp.json()
        more = False if resp.links.get("next") == None else True
        prev_resp = resp

        while more:
            curr_resp = self.session.get(prev_resp.links.get("next").get("url"))
            if resp.status_code != 200:
                logger.error(
                    "in while; get milestones status code is {}, expecting 200".format(
                        resp.status_code
                    )
                )
                exit(1)

            issues = issues + curr_resp.json()
            prev_resp = curr_resp
            more = False if curr_resp.links.get("next") == None else True

        logger.info("Number of issues for {}: {}".format(milestone, len(issues)))
        return issues
