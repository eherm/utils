"""Wrapper around the spork rest api issues resource
"""

import re

from typing import Optional
from pm import config

from ...models.issues import Issue
from ..session import SporkSession
from . import logger

from datetime import datetime


class SporkIssues:
    """Wrapper around spork issues"""

    def __init__(self):
        ss = SporkSession()
        self.session = ss.session
        self.horizon_gid = config.get("horizon_gid")
        self.url = "{}/groups/{}/issues".format(ss.url, self.horizon_gid, "issues")

    def get_issues(self) -> list[dict]:
        url = self.url
        payload = {"scope": "all"}
        resp = self.session.get(url, params=payload)
        if resp.status_code != 200:
            logger.error(
                "get issues status code is {}, expecting 200".format(resp.status_code)
            )
            exit(1)
        issues = resp.json()
        more = False if resp.links.get("next") == None else True
        prev_resp = resp
        while more:
            curr_resp = self.session.get(prev_resp.links.get("next").get("url"))
            if resp.status_code != 200:
                logger.error(
                    "in while; get issues status code is {}, expecting 200".format(
                        resp.status_code
                    )
                )
                exit(1)
            issues = issues + curr_resp.json()
            prev_resp = curr_resp
            more = False if curr_resp.links.get("next") == None else True

        return issues

    def get_ecr_label(self, issue: dict) -> Optional[str]:
        """Given a raw issue, return the ECR it belongs to or None if it doesn't belong to an ECR
        """
        ecr_re = re.compile(r".*(ECR-\d\d-[A-Z0-9]\d\d\d).*")
        for label in issue.get("labels"):
            match = ecr_re.search(label)
            if match:
                return match.group(1)
        return None

    def get_non_ecr_label(self, issue: dict) -> Optional[str]:
        """Given a raw issue, return the ECR it belongs to or None if it doesn't belong to an ECR
        """
        ecr_re = re.compile(r".*(ECR-\d\d-\d\d\d\d).*")
        for label in issue.get("labels"):
            match = ecr_re.search(label)
            if match:
                return match.group(1)
        return None

    def get_release_label(self, issue: dict) -> Optional[str]:
        """Given a raw issue, return the ECR release it belongs to or None if it doesn't belong to an ECR release
        """
        ecr_re = re.compile(r".*(ECR-\d\d-Q\d)")
        for label in issue.get("labels"):
            match = ecr_re.search(label)
            if match:
                return match.group(1)
        return None

    def filtered_issues(self, issues: list[dict]) -> list[Issue]:
        """Takes the raw issues from the api call and returns a list of :class:`Issue` objects

        Args:
            issues: a list of the raw issues from the git lab api

        Returns: a list of :class:`Issue`
        """
        filtered_issues: list[dict] = []
        for issue in issues:
            issue_dict = {}
            issue_dict["id"] = issue.get("id")
            issue_dict["title"] = issue.get("title")
            issue_dict["link"] = issue.get("web_url")
            issue_dict["closed"] = True if issue.get("state") == "closed" else False
            issue_dict["updated_at"] = datetime.fromisoformat(issue.get("updated_at"))
            ecr_label = self.get_ecr_label(issue)
            if ecr_label:
                issue_dict["ecr"] = ecr_label

            point = self.get_point_value(issue.get("labels"))
            if point and point > 0:
                issue_dict["estimated_points"] = point


            filtered_issues.append(Issue.issue(issue_dict))

        return filtered_issues

    def get_milestone_issues(self, milestone: str, issues: list[dict]) -> list[dict]:
        """Given a list of raw issues from spork, return only the issues matching the milestone

        Args:
            milestone: the name of the milestone 
            issues: the raw list of issues from the spork api call

        Returns: a subset of the issues list where each issue belongs to the target milestone
        """
        return [issue for issue in issues if issue.get("milestone") and issue.get("milestone").get("title") == milestone]

    def get_ecr_issues(self, issues: list[dict], ecr: Optional[str] = None) -> list[dict]:
        """Given a list of raw issues from spork, return only the issues matching the ecr or any ecr if ecr = None

        Args:
            ecr: the name of the ecr or None to match any ECR
            issues: the raw list of issues from the spork api call

        Returns: a subset of the issues list where each issue belongs to an ECR
        """
        ecr_issues: dict = []
        for issue in issues:
            ecr_label = self.get_ecr_label(issue)
            if ecr_label:
                if not ecr or ecr_label == ecr:
                    ecr_issues.append(issue)

        return ecr_issues

    def get_ecr_point_breakdown(self, issues: list[dict]) -> list[dict]:
        """Given a list of raw issues from spork, return only the issues matching the ecr or any ecr if ecr = None

        Args:
            ecr: the name of the ecr or None to match any ECR
            issues: the raw list of issues from the spork api call

        Returns: a subset of the issues list where each issue belongs to an ECR
        """
        ecr_issue_map: dict = {}
        for issue in issues:
            ecr_label = self.get_ecr_label(issue)
            if ecr_label:
                if not ecr_issue_map.get(ecr_label):
                    ecr_issue_map[ecr_label] = []
                ecr_issue_map[ecr_label].append(issue)

        ecr_point_map: dict = {ecr: self.calculate_points(issues) for ecr, issues in ecr_issue_map.items()}

        return ecr_point_map

    def get_ecr_point_breakdown_by_release(self, issues: list[dict], closed: bool = False) -> list[dict]:
        """Given a list of raw issues from spork, return a dictionary of the releases and the ecr points in 
        each release

        Args:
            issues: the raw list of issues from the spork api call
            closed: True to only return point values from closed tickets

        Returns: A dict in the below format

        {
            "23-Q2": {
                "ECR-23-0001": 3,
                "ECR-23-0002": 13
                },

            "23-Q3": {
                "ECR-23-0002": 7,
                "ECR-23-0005": 45
            }
        }
        """
        ecr_issue_map: dict = {}
        for issue in issues:
            ecr_label = self.get_ecr_label(issue)
            if ecr_label:
                release_label = self.get_release_label(issue)
                if not release_label:
                    logger.info(f"Release label not found for {issue.get('title')}")
                else:
                    r_label = release_label.split("-", 1)[1]
                    if not ecr_issue_map.get(r_label):
                        ecr_issue_map[r_label] = {}
                    if not ecr_issue_map.get(r_label).get(ecr_label):
                        ecr_issue_map.get(r_label)[ecr_label] = []
                    ecr_issue_map.get(r_label)[ecr_label].append(issue)

        #ecr_point_map: dict = {ecr: self.calculate_points(issues) for ecr, issues in ecr_issue_map.items()}

        ecr_point_map = {}
        for release, ecr_dict in ecr_issue_map.items():
            if closed:
                ecr_points: dict = {ecr: self.calculate_points_by_state(issues).get('closed') for ecr, issues in ecr_dict.items()}
            else:
                ecr_points: dict = {ecr: self.calculate_points(issues) for ecr, issues in ecr_dict.items()}
            ecr_point_map.update({release: ecr_points})


        return ecr_point_map

    def get_release_issues(self, release: str, issues: list[dict]) -> list[dict]:
        """Given a list of raw issues from spork, return only the issues matching the ecr

        Args:
            milestone: the name of the milestone 
            issues: the raw list of issues from the spork api call

        Returns: a subset of the issues list where each issue belongs to the target milestone
        """
        release_issues: list[dict] = []
        for issue in issues:
            release_label = self.get_release_label(issue)
            if release_label and release_label == release:
                release_issues.append(issue)

        return release_issues

    def get_point_value(self, labels):
        """Returns the point values for the issue

        Returns: a postive number if there is a valid point value
                a negative number if there is more than one point value assigned to the issues
                None if there is no point value assigned to the issue
        """
        point_map = {
            "1 Point": 1,
            "2 Points": 2,
            "3 Points": 3,
            "4 Points": 4,
            "5 Points": 5,
            "6 Points": 6,
        }

        if not labels:
            return None

        point = None
        for label in labels:
            if label in point_map.keys():
                if point:
                    return -1
                point = point_map.get(label)

        return point


    def handle_point_issues(self, point, title):
        if point == None:
            logger.info("Missing point value -> {}".format(title))
            return
        if point < 0:
            logger.info("Multiple point values assigned -> {}".format(title))


    def calculate_points_by_state(self, issues: list[dict]) -> dict:
        """Given a list of raw issues from the spork api calculate the points for each issue state

        Issues that do not have any valid point labels will be ignored

        valid point labels:
        
            1 Point
            2 Points
            3 Points
            4 Points
            5 Points
            6 Points

        Args:
            issues: list of issues from spork api
        
        Returns: A dict of the number of points mapped to the issue state
        """
        points = {"open": 0, "assigned": 0, "closed": 0}

        for issue in issues:
            state = issue.get("state")
            point = self.get_point_value(issue.get("labels"))

            if not point or point < 0:
                self.handle_point_issues(point, issue.get("title"))
                continue

            if state == "closed":
                points["closed"] = points["closed"] + point

            if state == "opened":
                if issue.get("assignee"):
                    points["assigned"] = points["assigned"] + point

                else:
                    points["open"] = points["open"] + point

        return points   

    def calculate_points(self, issues: list[dict]) -> int:
        """Given a list of raw issues from the spork api calculate the points from the labels
        Issues that do not have any valid point labels will be ignored

        valid point labels:
        
            1 Point
            2 Points
            3 Points
            4 Points
            5 Points
            6 Points

        Args:
            issues: list of issues from spork api
        
        Returns: the sum of the points from all the issues
        """
        
        points = self.calculate_points_by_state(issues)
        return sum(points.values())

