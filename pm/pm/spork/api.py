"""This is the API for the spork module
"""

from ..models.issues import Issue, Sprint
from .rest.issues import SporkIssues
from .rest.milestone import SporkMilestones

from typing import Optional


class Spork:
    def __init__(self):
        self.spork_issues = SporkIssues()
        self.spork_milestones = SporkMilestones()

    @property
    def issues(self) -> list[dict]:
        return self.spork_issues.get_issues()

    @property
    def filtered_issues(self) -> list[Issue]:
        issues = self.issues
        return self.spork_issues.filtered_issues(issues)


    @property
    def milestones(self):
        return self.spork_milestones.get_milestones()

    @property
    def sprints(self) -> list[Sprint]:
        s = self.milestones
        return self.spork_milestones.filter_milestones(s)

    def issues_in_milestone(self, milestone: str) -> list[Issue]:
        """Return a list of Issues that are part of a milestone

        Args:
            milestone: the name of the milestone

        Returns: A list of issues
        """
        issues = self.spork_milestones.get_milestone_issues(milestone)
        return self.spork_issues.filtered_issues(issues)

    def points_in_milestone(self, milestone:str) -> int:
        """
        """
        milestone_issues = self.spork_milestones.get_milestone_issues(milestone)
        return self.spork_issues.calculate_points(milestone_issues)

    def points_in_milestone_by_state(self, milestone:str) -> dict:
        """
        """
        milestone_issues = self.spork_milestones.get_milestone_issues(milestone)
        points_dict = self.spork_issues.calculate_points_by_state(milestone_issues)
        points_dict.update({"total": sum(points_dict.values())})
        return points_dict

    def points_in_milestone_by_ecr(self, milestone:str) -> dict:
        """
        """
        milestone_issues = self.spork_milestones.get_milestone_issues(milestone)
        points_dict = self.spork_issues.get_ecr_point_breakdown(milestone_issues)
        points_dict.update({"total": sum(points_dict.values())})
        return points_dict

    def issues_with_ecr(self, ecr: Optional[str] = None) -> list[Issue]:
        """Return a list of Issues that are part of an ECR

        Args:
            milestone: the ECR number or None to return all ECRs

        Returns: A list of issues
        """
        ecr_issues = self.spork_issues.get_ecr_issues(ecr, self.issues)
        return self.spork_issues.filtered_issues(ecr_issues)

    def points_with_ecr(self, ecr: Optional[str] = None) -> int:
        ecr_issues = self.spork_issues.get_ecr_issues(ecr, self.issues)
        return self.spork_issues.calculate_points(ecr_issues)

    def issues_with_ecr_in_milestone(self, ecr: str, milestone: str) -> list[Issue]:
        """
        """
        milestone_issues = self.spork_issues.get_milestone_issues(milestone, self.issues)
        ecr_issues = self.spork_issues.get_ecr_issues(ecr=ecr, issues=milestone_issues)
        return self.spork_issues.filtered_issues(ecr_issues)

    def points_with_ecr_in_milestone(self, milestone: str, ecr: Optional[str] = None) -> list[Issue]:
        """
        """
        milestone_issues = self.spork_milestones.get_milestone_issues(milestone)
        ecr_issues = self.spork_issues.get_ecr_issues(ecr=ecr, issues=milestone_issues)
        return self.spork_issues.calculate_points(ecr_issues)

    def points_with_ecr_in_milestone_by_release(self, milestone: str, closed: bool = False) -> list[Issue]:
        """
        """
        milestone_issues = self.spork_milestones.get_milestone_issues(milestone)
        ecr_issues = self.spork_issues.get_ecr_point_breakdown_by_release(issues=milestone_issues, closed=closed)
        return ecr_issues

    def get_sprint(self, milestone: str) -> Sprint:
        return self.spork_milestones.get_sprint(milestone)

    def issues_from_milestone(self, milestone: str):
        self.spork_milestones.get_milestone_issues(milestone)

    
