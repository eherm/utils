import json
import random
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from pm import config

from . import logger

DATA_FILE = config.get("data_file")
IMAGE_DIR = config.get("image_dir")
#DATA_FILE = "/home/eherman/Projects/utils/pm-core/scripts/sw/pm/pm/data.json"

def cal_sprints(data):
    """ return the sprints part of the data with calculations made

    assumes planned and completed velocity are filled out.

    computes meanVelocity, velocityVariance and velocityPredictability
    
    """

    sprints = data.get('sw').get('sprints')

    sprint_names = sorted(sprints.keys(), key=lambda x: int(x.replace("-","")))

    updated_sprints = {}

    completed_velocities = []

    for name in sprint_names:
        sprint = sprints.get(name)
        if completed_velocities:
            meanVelocity = round(sum(completed_velocities) / len(completed_velocities), 2)
            sprint['velocityVariance'] = meanVelocity - sprint.get('completedVelocity')
        else:
            sprint['velocityVariance'] = 0
            meanVelocity = 0

        sprint['meanVelocity'] = meanVelocity
        sprint['velocityPredictability'] = sprint.get('plannedVelocity') - sprint.get('completedVelocity')

        completed_velocities.append(sprint.get('completedVelocity'))

        updated_sprints[name] = sprint

        ecr_distro = {"releases": {}, "ecrs": {}}

        if sprint.get('releases'):
            ecr_total = 0
            non_ecr_total = 0
            for release_name, ecr_dict in sprint.get('releases').items():
                total = sum(ecr_dict.values())
                non_ecr = sum([point for ecr, point in ecr_dict.items() if 'N' in ecr])
                non_ecr_total = non_ecr_total + non_ecr
                ecr = total - non_ecr
                ecr_total = ecr_total + ecr
                ecr_distro.get("releases")[release_name] = {"total": total, "non_ecr": non_ecr, "ecr": ecr}
                
                for ecr_name, points in ecr_dict.items():
                    if not ecr_distro.get("ecrs").get(ecr_name):
                        ecr_distro.get("ecrs")[ecr_name] = 0
                    ecr_distro.get("ecrs")[ecr_name] = ecr_distro.get("ecrs").get(ecr_name) + points
            ecr_distro["totals"] = {"ecr": ecr_total, "non_ecr": non_ecr_total}
            
        sprint["ecr_distro"] = ecr_distro

    logger.debug(json.dumps(updated_sprints, indent=2))
    return updated_sprints


def graph_planned_completed_velocity(sprints):
    """create the completed velocity graph

    Args:
        sprints: the sprints data with the completed velocity populated
    """
    sprint_names = sorted(sprints.keys(), key=lambda x: int(x.replace("-","")))
    sprint_names = sprint_names[:-1]
    completed_velocities = [sprints.get(name).get('completedVelocity') for name in sprint_names]
    planned_velocities = [sprints.get(name).get('plannedVelocity') for name in sprint_names]

    fig, ax = plt.subplots(figsize=(12,8))
    plt.plot(sprint_names, completed_velocities, color='r', label='completed')
    plt.plot(sprint_names, planned_velocities, color='b', label='planned')
    plt.xlabel("Sprint", fontsize=10)
    plt.ylabel("Points", fontsize=12)
    plt.title("Planned vs Completed Velocities")
    plt.legend()
    plt.ylim(ymin=0)
    for i in range(len(sprint_names)):
        ax.text(sprint_names[i], completed_velocities[i], completed_velocities[i], size=10)

    plt.savefig("{}/planned_completed_velocity.png".format(IMAGE_DIR))

def graph_mean_completed_velocity(sprints):
    """create the completed velocity graph

    Args:
        sprints: the sprints data with the completed velocity populated
    """
    sprint_names = sorted(sprints.keys(), key=lambda x: int(x.replace("-","")))
    sprint_names = sprint_names[:-1]
    completed_velocities = [sprints.get(name).get('completedVelocity') for name in sprint_names]
    mean_velocities = [sprints.get(name).get('meanVelocity') for name in sprint_names]
    velocity_variance = [sprints.get(name).get('velocityVariance') for name in sprint_names]

    fig, ax = plt.subplots(figsize=(12,8))
    plt.plot(sprint_names, completed_velocities, color='r', label='completed')
    plt.plot(sprint_names, mean_velocities, color='g', label='mean')
    plt.xlabel("Sprint", fontsize=10)
    plt.ylabel("Points", fontsize=12)
    plt.title("Mean vs Completed Velocities")
    plt.legend()
    plt.ylim(ymin=0)
    for i in range(len(sprint_names)):
        ax.text(sprint_names[i], completed_velocities[i], completed_velocities[i], size=10)

    ax.text(sprint_names[-1], mean_velocities[-1], mean_velocities[-1], size=10)

    plt.savefig("{}/mean_completed_velocity.png".format(IMAGE_DIR))


def graph_velocity_variance(sprints):
    """create the completed velocity graph

    Args:
        sprints: the sprints data with the completed velocity populated
    """
    sprint_names = sorted(sprints.keys(), key=lambda x: int(x.replace("-","")))
    sprint_names = sprint_names[:-1]
    completed_velocities = [sprints.get(name).get('completedVelocity') for name in sprint_names]
    mean_velocities = [sprints.get(name).get('meanVelocity') for name in sprint_names]
    velocity_variance = [sprints.get(name).get('velocityVariance') for name in sprint_names]

    fig, ax = plt.subplots(figsize=(12,8))
    plt.plot(sprint_names, completed_velocities, color='r', label='completed')
    plt.plot(sprint_names, mean_velocities, color='g', label='mean')
    plt.xlabel("Sprint", fontsize=10)
    plt.ylabel("Points", fontsize=12)
    plt.title("Velocity Variance")
    ax.fill_between(sprint_names, completed_velocities, mean_velocities, color='y')
    plt.legend()
    plt.ylim(ymin=0)
    for i in range(len(sprint_names)):
        ax.text(sprint_names[i], completed_velocities[i], round(velocity_variance[i], 2), size=10)

    plt.savefig("{}/velocity_variance.png".format(IMAGE_DIR))



def graph_velocity_predictability(sprints):
    """create the completed velocity graph

    Args:
        sprints: the sprints data with the completed velocity populated
    """
    sprint_names = sorted(sprints.keys(), key=lambda x: int(x.replace("-","")))
    sprint_names = sprint_names[:-1]
    completed_velocities = [sprints.get(name).get('completedVelocity') for name in sprint_names]
    planned_velocities = [sprints.get(name).get('plannedVelocity') for name in sprint_names]
    velocity_predictability = [sprints.get(name).get('velocityPredictability') for name in sprint_names]


    fig, ax = plt.subplots(figsize=(12,8))
    plt.plot(sprint_names, completed_velocities, color='r', label='completed')
    plt.plot(sprint_names, planned_velocities, color='b', label='planned')
    plt.xlabel("Sprint", fontsize=10)
    plt.ylabel("Points", fontsize=12)
    plt.title("Velocity Predictability")
    ax.fill_between(sprint_names, completed_velocities, planned_velocities, color='y')
    plt.legend()
    plt.ylim(ymin=0)
    for i in range(len(sprint_names)):
        ax.text(sprint_names[i], completed_velocities[i], round(velocity_predictability[i], 2), size=10)

    plt.savefig("{}/velocity_predictability.png".format(IMAGE_DIR))



def graph_release_ecr_burndown(data, release_name):
    release = data.get('sw').get('releases').get(release_name)
    sprints = data.get('sw').get('sprints')
    
    sprint_names = release.get('sprints')
    estimatedPoints = release.get('estimatedPoints')
    expected_interval = estimatedPoints / len(sprint_names) + 1
    expected = [x * expected_interval for x in range(len(sprint_names), -1, -1)]
    expected = expected[1:]
    completed_sprints = []
    for name in sprint_names:
        if sprints.get(name):
            if sprints.get(name).get('completedVelocity', 0) > 0:
                completed_sprints.append(name)

    ecrs_completed = [sprints.get(name).get("ecr_distro").get("releases").get(release_name).get("total") for name in completed_sprints]

    running_sum = 0
    sum_list = []
    ecr_points_remaining = []
    ctr = 0
    for points in ecrs_completed:
        if not running_sum:
            running_sum = points
        else:
            running_sum = running_sum + points

        ecr_points_remaining.append(estimatedPoints - running_sum)
        ctr = ctr + 1

    plt.plot(sprint_names, expected, color='b')
    plt.ylim(ymin=0)

    plt.plot(completed_sprints, ecr_points_remaining, color='r')
    plt.xlabel("Sprint", fontsize=14)
    plt.ylabel("Story Points", fontsize=14)
    plt.title("ECR Burndown {}, Expected Points per sprint: {}".format(release_name, expected_interval))
    plt.ylim(ymin=0)

    plt.savefig("{}/ecr_burndown_{}.png".format(IMAGE_DIR, release_name))

def graph_quarter_burndown(data, quarter_name):
    release = data.get('sw').get('quarters').get(quarter_name)
    sprints = data.get('sw').get('sprints')
    
    sprint_names = release.get('sprints')
    estimatedPoints = release.get('estimatedPoints')
    expected_interval = estimatedPoints / len(sprint_names) + 1
    expected = [x * expected_interval for x in range(len(sprint_names), -1, -1)]
    expected = expected[1:]
    completed_sprints = []
    for name in sprint_names:
        if sprints.get(name):
            if sprints.get(name).get('completedVelocity', 0) > 0:
                completed_sprints.append(name)

    ecrs_completed = [sprints.get(name).get("ecr_distro").get("releases").get(release_name).get("total") for name in completed_sprints]

    running_sum = 0
    sum_list = []
    ecr_points_remaining = []
    ctr = 0
    for points in ecrs_completed:
        if not running_sum:
            running_sum = points
        else:
            running_sum = running_sum + points

        ecr_points_remaining.append(estimatedPoints - running_sum)
        ctr = ctr + 1

    plt.plot(sprint_names, expected, color='b')
    plt.ylim(ymin=0)

    plt.plot(completed_sprints, ecr_points_remaining, color='r')
    plt.xlabel("Sprint", fontsize=14)
    plt.ylabel("Story Points", fontsize=14)
    plt.title("ECR Burndown {}, Expected Points per sprint: {}".format(release_name, expected_interval))
    plt.ylim(ymin=0)

    plt.savefig("{}/ecr_burndown_{}.png".format(IMAGE_DIR, release_name))

def graph_quarter_burnup(data, quarter_name):
    quarter = data.get('sw').get('quarters').get(quarter_name)
    sprints = data.get('sw').get('sprints')
    
    sprint_names = quarter.get('sprints')
    estimatedPoints = quarter.get('estimatedPoints')
    expected_interval = estimatedPoints / len(sprint_names)
    expected = [x * expected_interval for x in range(len(sprint_names), -1, -1)]
    logger.debug(f"Quarter burnup expected: {expected}")
    expected.reverse()
    expected = expected[1:]
    logger.debug(f"Quarter burnup expected rev -1: {expected}")

    sum_sprints = []
    completed_sprints = []
    for name in sprint_names:
        if sprints.get(name):
            if sprints.get(name).get('completedVelocity', 0) > 0:
                completed_sprints.append(name)
                prev_points = sum_sprints[-1] if len(sum_sprints) > 0 else 0
                sum_sprints.append(prev_points + sprints.get(name).get('completedVelocity'))


    fig, ax = plt.subplots(figsize=(12,8))
    plt.plot(sprint_names, expected, color='b')
    plt.ylim(ymin=0)

    plt.plot(completed_sprints, sum_sprints, color='r')
    plt.xlabel("Sprint", fontsize=14)
    plt.ylabel("Story Points", fontsize=14)
    plt.title(f"Burnup {quarter_name}, Total Estimated {estimatedPoints}, Expected Points per sprint: {expected_interval}")
    plt.ylim(ymin=0)
    for i in range(len(completed_sprints)):
        ax.text(sprint_names[i], sum_sprints[i], sum_sprints[i], size=10)
    ax.text(sprint_names[-1], expected[-1], expected[-1], size=10)

    plt.savefig("{}/burnup_{}.png".format(IMAGE_DIR, quarter_name))

def graph_quarter_burnup_by_ecr(data, quarter_name):
    quarter = data.get('sw').get('quarters').get(quarter_name)
    sprints = data.get('sw').get('sprints')
    ecrs = data.get('sw').get('ecrs')

    points_by_ecr = {}
    for ecr_number, ecr_data in ecrs.items():
        ecr_sum = []
        completed_sprints = []
        sprint_names = quarter.get('sprints')
        for sprint_name in sprint_names:
            sprint = sprints.get(sprint_name)
            if sprint:
                if sprint.get('completedVelocity', 0) > 0:
                    completed_sprints.append(sprint_name)
                    if sprint.get('releases'):
                        combined_points = 0
                        for release, ecr_dict in sprint.get('releases').items():
                            combined_points = combined_points + ecr_dict.get(ecr_number, 0)
                        prev_sum = ecr_sum[-1] if len(ecr_sum) > 0 else 0
                        ecr_sum.append(prev_sum + combined_points)

        #points_by_ecr.update({ecr_number: ecr_sum})
        points_by_ecr.update({ecr_data.get('name'): ecr_sum})
        
        

        load = ecr_data.get('estimates', {}).get(quarter_name, 0)
        if load:
            expected_interval = load / len(sprint_names)
            expected = [x * expected_interval for x in range(len(sprint_names), -1, -1)]
            logger.debug(f"ECR {ecr_number} Quarter {quarter_name} burnup expected: {expected}")
            expected.reverse()
            expected = expected[1:]
            logger.debug(f"ECR {ecr_number} Quarter {quarter_name} burnup expected -1: {expected}")

        plt.cla()
        fig, ax = plt.subplots(figsize=(12,8))
        if load:
            plt.plot(sprint_names, expected, color='b')

        plt.plot(completed_sprints, ecr_sum, color='r')
        plt.xlabel("Sprint", fontsize=14)
        plt.ylabel("Story Points", fontsize=14)
        plt.ylim(ymin=0)
        rounded_interval = round(expected_interval, 2)
        plt.title(f"Burnup ECR {ecr_data.get('name')}:{quarter_name}, Total Estimated {load}, Expected Points per sprint: {rounded_interval}")
        plt.ylim(ymin=0)
        for i in range(len(completed_sprints)):
            ax.text(sprint_names[i], ecr_sum[i], ecr_sum[i], size=10)
        ax.text(sprint_names[-1], expected[-1], expected[-1], size=10)

        plt.savefig(f"{IMAGE_DIR}/burn_up-{ecr_number}-{quarter_name}.png")

    plt.cla()
    plt.close()

    fig, ax = plt.subplots()
    ax.stackplot(completed_sprints, points_by_ecr.values(), labels=points_by_ecr.keys())
    ax.legend(loc='upper left', reverse=True, fontsize=8)
    ax.set_xlabel("Sprint", fontsize=14)
    ax.set_ylabel("Story Points", fontsize=14)
    plt.savefig(f"{IMAGE_DIR}/ecr-stackplot-{quarter_name}.png")
    





def graph_velocity_distribution(sprints):
    """create the completed velocity graph

    Args:
        sprints: the sprints data with the completed velocity populated
    """
    sn1 = sorted(sprints.keys(), key=lambda x: int(x.replace("-","")))
    sn2 = sn1[:-1]
    sprint_names = sn2[16:]
    completed_velocities = [sprints.get(name).get('completedVelocity') for name in sprint_names]
    ecr_distros = {}
    for name in sprint_names:
        distro = sprints.get(name).get('ecr_distro').get('ecrs')
        for ecr_name in distro.keys():
            if not ecr_distros.get(ecr_name):
                ecr_distros[ecr_name] = []

    for name in sprint_names:
        distro = sprints.get(name).get('ecr_distro').get('ecrs')
        for dname in ecr_distros.keys():
            if not distro.get(dname):
                ecr_distros[dname].append(0)
            else:
                ecr_distros[dname].append(distro.get(dname))

    fig, ax = plt.subplots(figsize=(12,8))
    colors = ['b', 'g', 'r', 'c', 'm', 'y', 'k']
    for ecr_name, velocities in ecr_distros.items():
        plt.plot(sprint_names, velocities, color=random.choice(colors), label=ecr_name)
    
    plt.xlabel("Sprint", fontsize=10)
    plt.ylabel("Points", fontsize=12)
    plt.title("Velocity Distribution")
    plt.legend()
    plt.ylim(ymin=0)

    plt.savefig("{}/velocity_distribution.png".format(IMAGE_DIR))

def graph_velocity_distribution_non_vs_ecr(sprints):
    """create the completed velocity graph

    Args:
        sprints: the sprints data with the completed velocity populated
    """
    sn1 = sorted(sprints.keys(), key=lambda x: int(x.replace("-","")))
    sn2 = sn1[:-1]
    sprint_names = sn2[16:]
    completed_velocities = [sprints.get(name).get('completedVelocity') for name in sprint_names]
    logger.debug(f"Completed velocities: {completed_velocities}")

    ecr_velocities = [sprints.get(name).get('ecr_distro').get('totals').get('ecr')  for name in sprint_names]
    non_ecr_velocities = [sprints.get(name).get('ecr_distro').get('totals').get('non_ecr')  for name in sprint_names]

    logger.debug(f"len sprint names: {len(sprint_names)}")
    fig, ax = plt.subplots(figsize=(12,8))
    
    plt.plot(sprint_names, completed_velocities, color='r', label='Total points completed')
    plt.plot(sprint_names, non_ecr_velocities, color='b', label='Total Non-ECR points')
    plt.plot(sprint_names, ecr_velocities, color='g', label='Total ECR points')
    plt.xlabel("Sprint", fontsize=10)
    plt.ylabel("Points", fontsize=12)
    plt.title("Velocity Distribution")
    plt.legend()
    plt.ylim(ymin=0)
    for i in range(len(sprint_names)):
        ax.text(sprint_names[i], completed_velocities[i], completed_velocities[i], size=10)
        ax.text(sprint_names[i], non_ecr_velocities[i], non_ecr_velocities[i], size=10)
        ax.text(sprint_names[i], ecr_velocities[i], ecr_velocities[i], size=10)

    plt.savefig("{}/velocity_distribution_non_vs_ecr.png".format(IMAGE_DIR))

def graph_blockers(sprints):
    """create the completed velocity graph

    Args:
        sprints: the sprints data with the completed velocity populated
    """
    sprint_names = sorted(sprints.keys(), key=lambda x: int(x.replace("-","")))
    blockers = [sprints.get(name).get('blockers')  for name in sprint_names]

    fig, ax = plt.subplots(figsize=(12,8))
    plt.plot(sprint_names, blockers, color='c', label='Total blockers')
    plt.xlabel("Sprint", fontsize=10)
    plt.ylabel("Points", fontsize=12)
    plt.title("Blockers per sprint")
    plt.legend()
    plt.ylim(ymin=0)
    for i in range(len(sprint_names)):
        ax.text(sprint_names[i], blockers[i], blockers[i], size=10)

    plt.savefig("{}/blockers.png".format(IMAGE_DIR))

def run():
    data = {}
    with open(DATA_FILE) as f:
        data = json.load(f)

    sprints = cal_sprints(data)
    graph_planned_completed_velocity(sprints)
    plt.cla()
    plt.close()
    graph_mean_completed_velocity(sprints)
    plt.cla()
    plt.close()
    graph_velocity_variance(sprints)
    plt.cla()
    plt.close()
    graph_velocity_predictability(sprints)
    plt.cla()
    plt.close()
    graph_release_ecr_burndown(data, "23-Q3")
    plt.cla()
    plt.close()
    graph_velocity_distribution(sprints)
    plt.cla()
    plt.close()
    graph_velocity_distribution_non_vs_ecr(sprints)
    plt.cla()
    plt.close()
    graph_blockers(sprints)
    plt.cla()
    plt.close()
    graph_quarter_burnup(data, "23-Q3")
    plt.cla()
    plt.close()
    graph_quarter_burnup_by_ecr(data, "23-Q3")
    plt.close()


    logger.info("Complete")
    


if __name__ == "__main__":
    run()
