import os

config = {
    "base_url": "https://spork.navwar.fusion.navy.mil/api/v4",
    "horizon_gid": "124844",
    "release_file": "/home/dev/src/utils/pm-core/db/releases.csv",
    "ecr_file": "/home/dev/src/utils/pm-core/db/ecr_list.csv",
    "db": "/home/dev/src/utils/pm-core/db/sprint.db",
    "data_file": "/home/eherman/Projects/utils/pm/pm/data/data.json",
    "image_dir": "/home/eherman/Projects/utils/pm/pm/images",
    "log_level": "info"
}

if not os.environ.get("SPORK_TOKEN"):
    print("SPORK_TOKEN not set")
    exit(1)

token = {
    "spork_token": os.environ.get("SPORK_TOKEN")
}
