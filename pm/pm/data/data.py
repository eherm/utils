"""This is the API for the spork module
"""

from ..models import data as DataModels
from pm import config
import json

from typing import Optional

class Data:
    def __init__(self):
        self.data_file = config.get("data_file")

    def get_file(self):
        """ Return the file as python objects """
        jdata = {}
        with open(self.data_file) as f:
            jdata = json.load(f)

        return DataModels.SWData.load_from_dict(jdata)

    def update_sprint(self, sprint, completed_velocity = None, planned_velocity = None, releases = None, blockers = None):
        """ Load the data from the file and return an updated SWData with the new data

        Args:
            sprint: name of the sprint
            completed_velocity: the number of points completed
            planned_velocity: the number of points at the start of the sprint
            releases: A dict of releases that map to a dict of ECRs and their points completed in the sprint
            blockers: the number of blockers on that sprint

        Returns: An updated SWData object
        """
        data = self.get_file()
        if completed_velocity:
            data.sw.sprints.get(sprint).completedVelocity = completed_velocity
        if planned_velocity:
            data.sw.sprints.get(sprint).plannedVelocity = planned_velocity
        if releases:
            data.sw.sprints.get(sprint).releases = releases
        if blockers:
            data.sw.sprints.get(sprint).blockers = blockers

        return data


    def update_planned(self, data, updates):
        """ Take in the existing data and a list of updates and return the python objects """

    def save_file(self, data):
        """ Override the existing data file with the contents of the data argument """

