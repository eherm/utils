"""This module is the wrapper for the spork api
"""
import logging
from pm import config


log_level = config.get('log_level')
if log_level:
    if log_level == "notset":
        logging.basicConfig(level=logging.NOTSET)
    if log_level == "debug":
        logging.basicConfig(level=logging.DEBUG)
    if log_level == "info":
        logging.basicConfig(level=logging.INFO)
    if log_level == "warning":
        logging.basicConfig(level=logging.WARNING)
    if log_level == "error":
        logging.basicConfig(level=logging.ERROR)
    if log_level == "critical":
        logging.basicConfig(level=logging.CRITICAL)

logger = logging.getLogger(__name__)
