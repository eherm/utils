DROP schema public cascade;
create schema public;
    
CREATE TYPE area AS ENUM('Capability', 'Tech Debt', 'Infrastructure', 'Cyber/IA', 'Bug');
CREATE TYPE status AS ENUM('New', 'Investigating', 'Deferred', 'In Progress', 'Test', 'Closed - Fixed', 'Closed - OBE');
CREATE TYPE lmh AS ENUM('Low', 'LowMed', 'Med', 'MedHigh', 'High');
CREATE TYPE team AS ENUM('NIWC', 'Metron', 'Both');
