
CREATE TABLE release (
    id SERIAL PRIMARY KEY,
    quarter TEXT NOT NULL UNIQUE
);

CREATE TABLE person (
    id SERIAL PRIMARY KEY,
    person TEXT NOT NULL UNIQUE
);


CREATE TABLE ecrs (
    id SERIAL PRIMARY KEY,
    ecr_id TEXT UNIQUE,
    submitted_date DATE,
    area area,
    title TEXT,
    status status,
    priority lmh,
    complexity lmh,
    effort lmh,
    assigned team,
    release TEXT references release(quarter),
    submitted_by TEXT references person(person),
    originator TEXT references person(person),
    requested_priority lmh,
    requested_release TEXT references release(quarter),
    details TEXT
);

CREATE TABLE history (
    id SERIAL PRIMARY KEY,
    entered_date DATE NOT NULL,
    ecr int NOT NULL
);
