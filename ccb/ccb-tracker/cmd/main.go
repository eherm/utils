package main

import (
	"context"
	"fmt"
	"os"

	"github.com/jackc/pgx/v5"
)

func main() {

	conn, err := pgx.Connect(context.Background(), "postgresql://postgres:postgres@postgres:5432/postgres")

	if err != nil {
		fmt.Fprintf(os.Stderr, "unable to connect to database: %v\n", err)
		os.Exit(1)
	}

	defer conn.Close(context.Background())

	var greeting string

	err = conn.QueryRow(context.Background(), "select 'Hello, world!'").Scan(&greeting)

	if err != nil {
		fmt.Fprintf(os.Stderr, "QueryRow failed: %v\n", err)
		os.Exit(1)
	}

	_, err = conn.Prepare(context.Background(), "test", "INSERT INTO test VALUES ($1::int, $2::text)")

	if err != nil {
		fmt.Fprintf(os.Stderr, "Prepare failed: %v\n", err)
	}

	_, err = conn.Exec(context.Background(), "test", 3, "hi")

	if err != nil {
		fmt.Fprintf(os.Stderr, "Exec failed: %v\n", err)
	}

	var id int
	var name string

	err = conn.QueryRow(context.Background(), "Select id::int, name::text FROM test where id=$1", 3).Scan(&id, &name)

	if err != nil {
		fmt.Fprintf(os.Stderr, "Exec failed: %v\n", err)
	}

	fmt.Printf("ID: %d, NAME: %s", id, name)
}
